#!/usr/bin/ruby
$LOAD_PATH << File.dirname(__FILE__)
require '../lib/Auth'


# Test on SI
config = Auth.config("si_sis")


# puts ("This is word number: #{arraycounter} #{cipherlist[arraycounter]}")


# authenticate
path = config['url'] + "/authentication/login/"
response = RestClient.get(path, {params: {username: config['username'], password: config['password'], vendorkey: 'brightarrow'}})
authenticate = JSON.parse(response.body)
token = authenticate['Token']
# puts authenticate
# puts token


path = config['url'] + "/athletics/roster/"
response = RestClient.get(path, {params: {t: token, teamId: 3252694 }})
response = JSON.parse(response.body)
puts JSON.pretty_generate(response[0])


# json = STDIN.gets
# pretty_json = JSON::PrettyPrint.prettify json
# puts JSON.pretty_generate(response)
# puts JSON.pretty_generate response
# my_json = { :array => [1, 2, 3, { :sample => "hash"} ], :foo => "bar" }
# puts JSON.pretty_generate(my_json)

# puts JSON.prettify(response)

# path = url + "/user/phone/"
# RestClient.post "http://example.com/resource", {'x' => 1}.to_json, {content_type: :json, accept: :json}
# payload =
# '{
#   "UserId":2716843,
#   "TypeId":1712,
#   "PhoneId":null,
#   "PhoneNumber":"123-555-5555"
# }'
# response = RestClient.post(path, payload.to_json, {content_type: :json, accept: :json, params: {t: token}})
# puts response

# path = url + "/user/phone/"
# response = RestClient.get(path, {params: {t: token, schoolyearlabel: '2014 - 2015'}})
# puts response

# path = config['url'] + "/schoolinfo/term/"
# response = RestClient.get(path, {params: {t: token, schoolYear: '2017 - 2018'}})


# path = config['url'] + "/academics/section/"
# response = RestClient.get(path, {params: {t: token, schoolYear: '2017 - 2018', levelnum: 453, format: 'xml'}})
