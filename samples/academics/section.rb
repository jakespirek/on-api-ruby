#!/usr/bin/ruby
$LOAD_PATH << File.dirname(__FILE__)

require 'yaml'
require 'json'
require 'csv'
require 'rest-client'

config = YAML.load_file('../../config.yml')

# https://github.com/rest-client/rest-client

path = config['url'] + "/authentication/login/"
response = RestClient.get(path, {params: {username: config['username'], password: config['password']}})
authenticate = JSON.parse(response.body)
token = authenticate['Token']
# puts authenticate
puts token


path = config['url'] + "/schoolinfo/schoollevel/"
response = RestClient.get(path, {params: {t: token}})

# path = config['url'] + "/schoolinfo/term/"
# response = RestClient.get(path, {params: {t: token, schoolYear: '2017 - 2018'}})


path = config['url'] + "/academics/section/"
response = RestClient.get(path, {params: {t: token, schoolYear: '2017 - 2018', levelnum: 453, format: 'xml'}})

# puts response
