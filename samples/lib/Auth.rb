#!/usr/bin/ruby

class Auth

  require 'yaml'
  require 'json'
  require 'csv'
  require 'rest-client'


  # def self.config(environment)
  #   config_file = YAML.load_file('../../config.yml')
  #   env = config_file[environment]
  #   config = {'url' => env['url'] + "/api", 'username' => env['username'], 'password' => env['password']}
  #   return config
  # end

def self.authenticate(environment, vendorkey="")
  config_file = YAML.load_file('../../config.yml')
  env = config_file[environment]
  url = env['url'] + "/api"
  path = "/authentication/login/"
  if vendorkey != ""
    puts "vendor key is not null"
    response = RestClient.get( url + path, {params: {username: env['username'], password: env['password'], vendorkey: vendorkey}} )
  else
    response = RestClient.get( url + path, {params: {username: env['username'], password: env['password']}} )
  end
  authenticated = JSON.parse(response.body)

  return config = { 'url' => url, 'username' => env['username'], 'password' => env['password'], 'token' => authenticated['Token'] }
end


  def self.confirmPost(config)
    puts "This is a POST function on #{config['url']}. Are you sure you want to continue? (hit y)"
    continue = gets.chomp
    if continue != "y"
      return
    end
  end

  def self.to_csv
    attributes = %w{zendesk_id name account_manager salesforce_id clarify_site_id school_id success_coach owns_core owns_core owns_ob owns_oc owns_om owns_or account_executive_id created_at updated_at}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |school|
        csv << attributes.map{ |attr| school.send(attr) }
      end
    end
  end


  def self.parse_from_csv(csv_document)
    # http://ruby-doc.org/stdlib-1.9.3/libdoc/csv/rdoc/CSV.html
    rows = []
    CSV.foreach(csv_document, headers: true) do |row|
      # Push the row as a hash into the rows array
      rows << row.to_hash
    end
    return rows
  end



end
