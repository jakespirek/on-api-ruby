#!/usr/bin/ruby
$LOAD_PATH << File.dirname(__FILE__)
require '../lib/Auth'

environment = "sb"
config = Auth.authenticate(environment)
Auth.confirmPost(config)


# User1Id = parent
# User2Id = child
path = "/user/relationship/"
payload =
'{
  "Relationship": 6,
  "RelationshipId": 6,
  "RelationshipTypeId": 2,
  "User1Id": 2716843,
  "User2Id": 2861920,
  "ParentalAccess": false,
  "ListAsParent": false,
  "TuitionResponsibleSigner": false
}'
response = RestClient.post(config['url'] + path, payload, {content_type: :json, accept: :json, params: {t: config['token']}})
response = JSON.parse(response.body)
puts JSON.pretty_generate(response)
