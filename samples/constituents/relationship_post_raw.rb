#!/usr/bin/ruby
$LOAD_PATH << File.dirname(__FILE__)
require '../lib/Auth'

environment = "sb"
config = Auth.authenticate(environment)
# Auth.confirmPost(config)

require 'net/http'
require 'uri'
require 'json'

uri = URI.parse("https://25165.myschooltraining.com/api/user/relationship?t=#{config['token']}")
request = Net::HTTP::Post.new(uri)
request.content_type = "application/json; charset=utf-8"
request.body = JSON.dump({
  "Relationship" => 6,
  "RelationshipId" => 6,
  "RelationshipTypeId" => 2,
  "User1Id" => 2716843,
  "User2Id" => 2861920,
  "ParentalAccess" => false,
  "ListAsParent" => false,
  "TuitionResponsibleSigner" => false
})

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end

puts response.code
puts response.body
