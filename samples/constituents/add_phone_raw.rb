#!/usr/bin/ruby
$LOAD_PATH << File.dirname(__FILE__)
require '../lib/Auth'

environment = "sb"
config = Auth.authenticate(environment)


require 'net/http'
require 'uri'
require 'json'

uri = URI.parse("https://25165.myschooltraining.com/api/user/PhoneForUser?t=#{config['token']}")
request = Net::HTTP::Post.new(uri)
request.content_type = "application/json; charset=utf-8"
request.body = JSON.dump({
  "UserId" => 2716843,
  "PhoneNumber" => "123-456-7891",
  "TypeId" => 2614
})

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end

puts response.code
puts response.body
