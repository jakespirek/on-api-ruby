#!/usr/bin/ruby
$LOAD_PATH << File.dirname(__FILE__)
require '../lib/Auth'

environment = "sb"
config = Auth.authenticate(environment)

puts "This is a POST function on #{config['url']}. Are you sure you want to continue? (hit y)"
continue = gets.chomp
if continue != "y"
  return
end

path = "/user/PhoneForUser/"
payload =
'{
  "UserId":2716843,
  "TypeId":2614,
  "PhoneNumber":"123-555-5555"
}'
response = RestClient.post(config['url'] + path, payload, {content_type: :json, accept: :json, params: {t: config['token']}})
response = JSON.parse(response.body)
puts JSON.pretty_generate(response)
